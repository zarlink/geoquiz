package com.example.zlnk.geoquiz.models;

/**
 * Created by zlnk on 11/11/15.
 */
public class Question {

    private int mTextResId;     /*  ¿Por que es un entero?*/
    private boolean mAnswerTrue;


    /*  Creamos el constructor  */
    public Question(int textResId, boolean answerTrue){
        mTextResId = textResId;
        mAnswerTrue = answerTrue;
    }

        /*  Getter generado dinamicamente   */
    public boolean isAnswerTrue() {
        return mAnswerTrue;
    }

    public int getTextResId() {
        return mTextResId;
    }

        /*  Setter generado dinamicamente   */
    public void setTextResId(int textResId) {
        mTextResId = textResId;
    }

    public void setAnswerTrue(boolean answerTrue) {
        mAnswerTrue = answerTrue;
    }
}
